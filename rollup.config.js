"use strict";

import clear from "rollup-plugin-clear";
import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import typescript from "rollup-plugin-typescript2";

export default [
  {
    input: "webroot/ts/main.ts",
    output: {
      file: "webroot/js/antispam.js",
      format: "iife",
      sourcemap: true,
      name: 'antispam'
    },

    plugins: [
      clear({ targets: ["dist"] }),
      resolve(),
      commonjs(),
      typescript({ tsconfig: "./tsconfig.json" }),
    ]
  },
  {
    input: "webroot/ts/worker.ts",
    output: {
      file: "webroot/js/worker.js",
      format: "iife",
      sourcemap: true,
      name: 'worker'
    },

    plugins: [
      clear({ targets: ["dist"] }),
      resolve(),
      commonjs(),
      typescript({ tsconfig: "./tsconfig.json" }),
    ]
  }
]
