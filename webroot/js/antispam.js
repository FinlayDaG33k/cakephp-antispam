var antispam = (function (exports) {
    'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    const DEFAULT_REQUEST_OPTIONS = {
        ignoreCache: false,
        headers: {
            Accept: 'application/json, text/javascript, text/plain',
        },
        // default max duration for a request
        timeout: 5000,
    };
    function queryParams(params = {}) {
        return Object.keys(params)
            .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
            .join('&');
    }
    function withQuery(url, params = {}) {
        const queryString = queryParams(params);
        return queryString ? url + (url.indexOf('?') === -1 ? '?' : '&') + queryString : url;
    }
    function parseXHRResult(xhr) {
        return {
            ok: xhr.status >= 200 && xhr.status < 300,
            status: xhr.status,
            statusText: xhr.statusText,
            headers: xhr.getAllResponseHeaders(),
            data: xhr.responseText,
            json: () => JSON.parse(xhr.responseText),
        };
    }
    function errorResponse(xhr, message = null) {
        return {
            ok: false,
            status: xhr.status,
            statusText: xhr.statusText,
            headers: xhr.getAllResponseHeaders(),
            data: message || xhr.statusText,
            json: () => JSON.parse(message || xhr.statusText),
        };
    }
    function request(method, url, queryParams = {}, body = null, options = DEFAULT_REQUEST_OPTIONS) {
        // Parse our options
        const ignoreCache = options.ignoreCache || DEFAULT_REQUEST_OPTIONS.ignoreCache;
        const headers = options.headers || DEFAULT_REQUEST_OPTIONS.headers;
        const timeout = options.timeout || DEFAULT_REQUEST_OPTIONS.timeout;
        return new Promise((resolve, reject) => {
            // Create our new XHR
            const xhr = new XMLHttpRequest();
            // Set the method and query parameters
            xhr.open(method, withQuery(url, queryParams));
            // Set our headers
            if (headers)
                Object.keys(headers).forEach(key => xhr.setRequestHeader(key, headers[key]));
            // Set whether we want to use a cache
            if (ignoreCache)
                xhr.setRequestHeader('Cache-Control', 'no-cache');
            // Set out timeout
            xhr.timeout = timeout;
            xhr.onload = () => {
                resolve(parseXHRResult(xhr));
            };
            xhr.onerror = () => {
                resolve(errorResponse(xhr, 'An error occured while making the request.'));
            };
            xhr.ontimeout = () => {
                resolve(errorResponse(xhr, 'Request timed out.'));
            };
            if (method === 'post' && body) {
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.send(JSON.stringify(body));
            }
            else {
                xhr.send();
            }
        });
    }

    // Found on https://stackoverflow.com/a/48161723/5001849
    function sha256string(message) {
        return __awaiter(this, void 0, void 0, function* () {
            // encode as UTF-8
            const msgBuffer = new TextEncoder().encode(message);
            // hash the message
            const hashBuffer = yield crypto.subtle.digest('SHA-256', msgBuffer);
            // convert ArrayBuffer to Array
            const hashArray = Array.from(new Uint8Array(hashBuffer));
            // convert bytes to hex string                  
            const hashHex = hashArray.map(b => ('00' + b.toString(16)).slice(-2)).join('');
            return hashHex;
        });
    }

    function asyncForEach(array, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            for (let index = 0; index < array.length; index++) {
                yield callback(array[index], index, array);
            }
        });
    }

    class Challenge {
        constructor() {
            this.solution = '';
            this.nonce = '';
        }
        get() {
            return __awaiter(this, void 0, void 0, function* () {
                // Obtain challenges from the server
                let r = yield request('get', '/antispam/getchallenge');
                // Check if we had an error
                if (!r.status)
                    throw new Error("Could not obtain a challenge!");
                // deserialize the data
                let data = JSON.parse(r.data);
                // Store our nonce
                this.nonce = data.nonce;
                // return the challenge data
                return data;
            });
        }
        submit(tokens, csrfToken) {
            return __awaiter(this, void 0, void 0, function* () {
                // Hash our tokens
                let hashes = [];
                yield asyncForEach(tokens, (token) => __awaiter(this, void 0, void 0, function* () {
                    hashes.push(yield sha256string(token));
                }));
                // Create our data object
                let data = {
                    nonce: this.nonce,
                    solution: this.solution,
                    tokens: hashes
                };
                // Set request options
                const options = {
                    headers: {
                        'X-CSRF-Token': csrfToken
                    }
                };
                // Submit everything and get the response
                let r = yield request('post', '/antispam/sendsolution', {}, data, options);
                // Get all the signatures
                let resp = JSON.parse(r.data);
                // Check if our challenge was valid
                if (!resp.success)
                    return [];
                // Match the signatures with the tokens
                let signedTokens = [];
                for (let signature of resp.signatures) {
                    signedTokens.push({
                        token: tokens[resp.signatures.indexOf(signature)],
                        signature: signature.signature,
                        ts: signature.data.timestamp
                    });
                }
                return signedTokens;
            });
        }
    }

    class Token {
        constructor() {
            this.tokens = [];
        }
        generate() {
            var arr = new Uint8Array(64);
            let rand = window.crypto.getRandomValues(arr);
            if (!rand)
                throw new Error("Could not generate random values!");
            let token = sha256string(rand.toString());
            return token;
        }
        count() {
            let tokens = this.get(null);
            if (!tokens)
                return 0;
            return tokens.length;
        }
        get(amount = 1) {
            // Get our tokens from the local storage
            let data = localStorage.getItem('antispam-tokens');
            if (!data)
                return null;
            let tokens = JSON.parse(data);
            if (!tokens)
                return null;
            // Get our tokens
            let validTokens = [];
            for (let token of tokens) {
                // Check if the token has expired
                var now = new Date();
                var expiry = new Date((parseInt(token.ts) + 3600) * 1000);
                if (now < expiry)
                    validTokens.push(token);
                // Check if we have specified a required amount
                if (!amount === null) {
                    // Specified amount
                    // Check if we have enough
                    if (validTokens.length == amount)
                        break;
                }
            }
            return validTokens;
        }
        remove(token) {
            return __awaiter(this, void 0, void 0, function* () {
                // Get our tokens frok the LocalStorage
                let data = localStorage.getItem('antispam-tokens');
                if (!data)
                    return;
                // Parse the tokens object
                let tokens = JSON.parse(data);
                if (!tokens)
                    return;
                // Find the specified token
                let index = tokens.findIndex((t) => t.token === token.token);
                if (index === -1)
                    return;
                // Remove it from the object
                tokens.splice(index, 1);
                // Save the tokens
                localStorage.setItem('antispam-tokens', JSON.stringify(tokens));
            });
        }
    }

    class AntiSpam {
        constructor() {
            this.challenge = new Challenge();
            this.token = new Token();
            this.csrf = '';
        }
        solve() {
            return __awaiter(this, void 0, void 0, function* () {
                // Create a new WebWorker
                var myWorker = new Worker('/finlay_da_g33k/anti_spam/js/worker.js');
                // Get a challenge and solve it
                let solution = yield new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                    // Get a challenge
                    let data = yield this.challenge.get();
                    // Let the webworker solve
                    myWorker.postMessage([data]);
                    // Wait for a solution to come back
                    myWorker.onmessage = (e) => {
                        // Send the solution to our challenge instange
                        this.challenge.solution = e.data;
                        // Resolve
                        resolve(true);
                    };
                }));
                // Generate our 5 tokens
                this.token.tokens = [];
                for (let i = 0; this.token.tokens.length < 5; i++) {
                    this.token.tokens.push(yield this.token.generate());
                }
                // Submit our tokens
                let tokens = yield this.challenge.submit(this.token.tokens, this.csrf);
                // Store our tokens
                localStorage.setItem('antispam-tokens', JSON.stringify(tokens));
            });
        }
        countTokens() {
            return this.token.count();
        }
        getTokens(amount = 1) {
            return this.token.get(amount);
        }
    }

    exports.AntiSpam = AntiSpam;

    return exports;

}({}));
//# sourceMappingURL=antispam.js.map
