import { sha256string } from 'util/sha256';

export class Token {
  public tokens: any[string] = [];
  
  public generate() {
    var arr = new Uint8Array(64);
    let rand = window.crypto.getRandomValues(arr);
    if(!rand) throw new Error("Could not generate random values!");

    let token = sha256string(rand.toString());
    return token;
  }

  public count() {
    let tokens = this.get(null);
    if(!tokens) return 0;
    return tokens.length;
  }

  public get(amount: number|null = 1) {
    // Get our tokens from the local storage
    let data = localStorage.getItem('antispam-tokens');
    if(!data) return null;
    let tokens: any[] = JSON.parse(data);
    if(!tokens) return null;

    // Get our tokens
    let validTokens = [];
    for(let token of tokens) {
      // Check if the token has expired
      var now = new Date();
      var expiry = new Date((parseInt(token.ts) + 3600) * 1000);
      if(now < expiry) validTokens.push(token);

      // Check if we have specified a required amount
      if(!amount === null) {
        // Specified amount
        // Check if we have enough
        if(validTokens.length == amount) break;
      }
    }

    return validTokens;
  }

  public async remove(token: any) {
    // Get our tokens frok the LocalStorage
    let data = localStorage.getItem('antispam-tokens');
    if(!data) return;

    // Parse the tokens object
    let tokens = JSON.parse(data);
    if(!tokens) return;

    // Find the specified token
    let index = tokens.findIndex((t: any) => t.token === token.token);
    if(index === -1) return;

    // Remove it from the object
    tokens.splice(index, 1);

    // Save the tokens
    localStorage.setItem('antispam-tokens', JSON.stringify(tokens));
  }
}