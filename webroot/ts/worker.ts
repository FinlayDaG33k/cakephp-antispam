import { sha256string } from 'util/sha256';

onmessage = async function(e: any) {
  // Start trying each permutation until we find a solution
  let solution = '';

  // Loop over each option given by the server
  // Break when a solution was found
  for await(let option of e.data[0]['options']) {
    // Hash our option
    let h = await sha256string(`${e.data[0]['nonce']}${option}`);

    // Check if we've found the solution
    if(h == e.data[0]['challenge']) {
      console.log(`Found solution after ${e.data[0]['options'].indexOf(option) + 1} attempts: ${option}`);
      solution = option;
      break;
    }
  }
  
  postMessage(solution);
}