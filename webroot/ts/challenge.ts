import { request, RequestOptions } from 'util/xhr';
import { sha256string } from 'util/sha256';
import { asyncForEach } from 'util/asyncForEach';

export class Challenge {
  public solution: string = '';
  public nonce: string = '';
  
  public async get() {
    // Obtain challenges from the server
    let r = await request('get', '/antispam/getchallenge');

    // Check if we had an error
    if(!r.status) throw new Error("Could not obtain a challenge!");

    // deserialize the data
    let data = JSON.parse(r.data);

    // Store our nonce
    this.nonce = data.nonce;

    // return the challenge data
    return data;
  }

  public async submit(tokens: any[string], csrfToken: string) {
    // Hash our tokens
    let hashes: any[] = [];
    await asyncForEach(tokens, async (token: string) => {
      hashes.push(await sha256string(token));
    });

    // Create our data object
    let data = {
      nonce: this.nonce,
      solution: this.solution,
      tokens: hashes
    };

    // Set request options
    const options = {
      headers: {
        'X-CSRF-Token': csrfToken
      }
    };

    // Submit everything and get the response
    let r = await request('post', '/antispam/sendsolution', {}, data, options);

    // Get all the signatures
    let resp = JSON.parse(r.data);

    // Check if our challenge was valid
    if(!resp.success) return [];

    // Match the signatures with the tokens
    let signedTokens = [];
    for(let signature of resp.signatures) {      
      signedTokens.push({
        token: tokens[resp.signatures.indexOf(signature)],
        signature: signature.signature,
        ts: signature.data.timestamp
      });
    }

    return signedTokens;
  }
}