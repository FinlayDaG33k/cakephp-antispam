import { Challenge } from 'challenge';
import { Token } from 'token';

export class AntiSpam {
  challenge: Challenge = new Challenge();
  public token: Token = new Token();
  public csrf: string = '';

  public async solve() {
    // Create a new WebWorker
    var myWorker = new Worker('/finlay_da_g33k/anti_spam/js/worker.js');

    // Get a challenge and solve it
    let solution = await new Promise(async (resolve, reject) => {
      // Get a challenge
      let data = await this.challenge.get();
    
      // Let the webworker solve
      myWorker.postMessage([data]);
    
      // Wait for a solution to come back
      myWorker.onmessage = (e) => {
        // Send the solution to our challenge instange
        this.challenge.solution = e.data;
    
        // Resolve
        resolve(true);
      }
    });

    // Generate our 5 tokens
    this.token.tokens = [];
    for(let i: number = 0; this.token.tokens.length <5; i++) {
      this.token.tokens.push(await this.token.generate());
    }

    // Submit our tokens
    let tokens = await this.challenge.submit(this.token.tokens, this.csrf);

    // Store our tokens
    localStorage.setItem('antispam-tokens', JSON.stringify(tokens));
  }

  public countTokens() {
    return this.token.count();
  }

  public getTokens(amount: number = 1) {
    return this.token.get(amount);
  }
}
