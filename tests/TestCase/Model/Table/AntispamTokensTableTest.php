<?php
namespace FinlayDaG33k\AntiSpam\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use FinlayDaG33k\AntiSpam\Model\Table\AntispamTokensTable;

/**
 * FinlayDaG33k\AntiSpam\Model\Table\AntispamTokensTable Test Case
 */
class AntispamTokensTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \FinlayDaG33k\AntiSpam\Model\Table\AntispamTokensTable
     */
    public $AntispamTokens;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.FinlayDaG33k/AntiSpam.AntispamTokens'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AntispamTokens') ? [] : ['className' => AntispamTokensTable::class];
        $this->AntispamTokens = TableRegistry::getTableLocator()->get('AntispamTokens', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AntispamTokens);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
