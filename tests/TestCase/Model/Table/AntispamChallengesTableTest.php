<?php
namespace FinlayDaG33k\AntiSpam\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use FinlayDaG33k\AntiSpam\Model\Table\AntispamChallengesTable;

/**
 * FinlayDaG33k\AntiSpam\Model\Table\AntispamChallengesTable Test Case
 */
class AntispamChallengesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \FinlayDaG33k\AntiSpam\Model\Table\AntispamChallengesTable
     */
    public $AntispamChallenges;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.FinlayDaG33k/AntiSpam.AntispamChallenges'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AntispamChallenges') ? [] : ['className' => AntispamChallengesTable::class];
        $this->AntispamChallenges = TableRegistry::getTableLocator()->get('AntispamChallenges', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AntispamChallenges);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
