# Development

## Requirements
Before you get started, please make sure you have installed the following:
- nodejs (>=12)
- yarn

## Cloning this repo

## Building the JavaScript
You can build the JavaScript files by using the following command:
> yarn run build

After this is done, the files will automatically be placed in `webroot/js` for you to use!