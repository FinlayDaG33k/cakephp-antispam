<?php
  namespace FinlayDaG33k\AntiSpam\Controller;

  use FinlayDaG33k\AntiSpam\Controller\AppController;
  use Cake\Event\Event;
  use FinlayDaG33k\AntiSpam\{
    Challenge,
    Token
  };
  use Cake\Utility\Security;

  class ApiController extends AppController {
    private $challenge;

    public function initialize() {
      parent::initialize();

      // Intantiate a Challenge class
      $this->challenge = new Challenge();

      // Load the JSON component
      $this->loadComponent('Json');
    }

    public function beforeFilter(Event $event) {
      $this->Auth->allow();
    }

    public function getChallenge() {
      // Create our challenge
      $challenge = $this->challenge->create();

      // Hash the challenge data
      $hash = Security::hash($challenge->nonce . $challenge->solution, 'sha256');

      // Generate a list of options
      $options = [$challenge->solution];
      while(count($options) < 10) {
        $options[] = Security::randomString(3);
      }

      // Shuffle the combinations
      shuffle($options);

      // Send the challenge details back to the client
      $this->Json->sendResponse([
        'nonce' => $challenge->nonce,
        'timestamp' => $challenge->timestamp,
        'challenge' => $hash,
        'options' => $options,
      ]);
    }

    public function sendSolution() {
      // Verify the solution
      $result = $this->challenge->verify($this->request->getData('nonce'), $this->request->getData('solution'));
      if(!$result) {
        $this->Json->sendResponse([
          'success' => false,
          'signatures' => [],
        ]);
      }
      
      // Solution was verified
      // Create a new token instance
      $token = new Token();

      // Sign our tokens
      $signatures = $token->sign($this->request->getData('tokens'));

      // Send the tokens back to the client
      $this->Json->sendResponse([
        'success' => true,
        'signatures' => $signatures,
      ]);
    }
  }