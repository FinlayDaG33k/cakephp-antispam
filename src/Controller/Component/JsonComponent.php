<?php
  namespace FinlayDaG33k\AntiSpam\Controller\Component;

  use Cake\Controller\Component;

  class JsonComponent extends Component {
    private $_statusCode = 200;
    private $_data = [];

    public function setData(array $data = []) {
      // set the data property
      if(!empty($data)) {
        $this->_data = $data;
      }

      return $this;
    }

    public function setStatus(int $status = 200) {
      // set the status property
      if(!$status == 200) {
        $this->_statusCode = $status;
      }

      return $this;
    }

    public function sendResponse(array $responseData = [], int $responseStatusCode = 200) {
      // Populate the properties
      if(!empty($responseData)) {
        $this->_data = $responseData;
      }

      if(!$responseStatusCode == 200) {
        $this->_data = $responseStatusCode;
      }

      // Create the json response
      $this->response->type('json');
      $this->response->statusCode($responseStatusCode);
      $this->response->body(json_encode($this->_data));

      // send the response
      return $this->getController()->response->send();
    }
  }