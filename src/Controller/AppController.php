<?php
  namespace FinlayDaG33k\AntiSpam\Controller;

  use App\Controller\AppController as BaseController;

  class AppController extends BaseController {
    public function initialize() {
      parent::initialize();
    }
  }