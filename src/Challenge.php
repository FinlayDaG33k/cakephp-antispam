<?php
  namespace FinlayDaG33k\AntiSpam;

  use Cake\ORM\TableRegistry;
  use Cake\Utility\Security;
  use Cake\I18n\Time;
  
  class Challenge {
    private $_nonce = '';
    private $_solution = '';
    private $_timestamp = 0;

    public function __construct() {
      // Load the required models
      $this->AntispamChallengesTable = TableRegistry::get('AntispamChallenges');
    }

    public function create() {
      // Create a new challenge
      $this->_nonce = Security::randomString(8);
      $this->_solution = Security::randomString(3);
      $this->_timestamp = Time::now()->toUnixString();

      // Create a new table entity
      $entity = $this->AntispamChallengesTable->newEntity([
        'nonce' => $this->_nonce,
        'solution' => $this->_solution,
        'timestamp' => $this->_timestamp,
      ]);

      // Save the entity
      if(!$this->AntispamChallengesTable->save($entity)) throw new \Exception("Could not save challenge!");

      // Entity saves
      return $entity;
    }

    public function verify(string $nonce, string $solution): bool {
      // Find the challenge
      $challenge = $this->AntispamChallengesTable->findByNonce($nonce)->first();

      // Check if a challenge was found
      if(!$challenge) return false;

      // Challenge was found
      // Check whether it was solved on time
      $now = Time::now();
      $expiry = $challenge->timestamp->addMinute(1);
      if($now > $expiry) return false;

      // Check whether the solution was valid
      if($challenge->solution != $solution) return false;

      // Solution was valid
      // Remove it from the tables
      $this->AntispamChallengesTable->delete($challenge);

      // Everything went alright
      return true;
    }
  }