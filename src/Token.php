<?php
  namespace FinlayDaG33k\AntiSpam;

  use Cake\I18n\Time;
  use Cake\Utility\Security;
  use Cake\ORM\TableRegistry;

  class Token {
    public function __construct() {
      // Load the required models
      $this->AntispamTokensTable = TableRegistry::get('AntispamTokens');
    }

    public function sign(array $tokens) {
      // Make sure we don't cheat in the token count
      if(count($tokens) > 5) $tokens = array_slice($tokens, 0, 5, true);

      // Process each token
      $signatures = [];
      foreach($tokens as $token) {
        // Get a timestamp
        $ts = Time::now()->toUnixString();

        // Create our data
        $data = '';
        $data .= $token;
        $data .= $ts;

        // Create our signature object
        $obj = [
          'data' => [
            'token' => $token,
            'timestamp' => $ts,
          ],
          'signature' => Security::hash($data, 'sha256', true),
        ];

        // Save the token to prevent double spending
        $entity = $this->AntispamTokensTable->newEntity([
          'token' => $token,
          'timestamp' => $ts
        ]);

        if($this->AntispamTokensTable->save($entity)) $signatures[] = $obj;
      }

      // Return all the signed tokens
      return $signatures;
    }

    public function validate($input) {
      // Decode the token JSON
      $data = json_decode($input, 1);

      // Hash our token so we can validate it
      $token = Security::hash($data['token'], 'sha256', false);

      // Create the signature
      $signature = Security::hash($token . $data['ts'], 'sha256', true);

      // Check if the signature is valid
      if($signature != $data['signature']) return false;

      // Check whether the token is in our database
      $entity = $this->AntispamTokensTable->findByToken($token)->first();
      if(!$entity) return false;

      // Check if the token has expired
      if($entity->timestamp->addHour(1) < Time::now()) {
        // Remove the token from the database
        $this->AntispamTokensTable->delete($entity);
        return false;
      }

      // Remove the token from the database
      $this->AntispamTokensTable->delete($entity);
      return true;
    }
  }