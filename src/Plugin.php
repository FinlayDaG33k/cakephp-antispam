<?php
  namespace FinlayDaG33k\AntiSpam;

  use Cake\Core\BasePlugin;
  use Cake\Core\PluginApplicationInterface;

  class Plugin extends BasePlugin {
    public function bootstrap(PluginApplicationInterface $app) {
      // Add constants, load configuration defaults.
      // By default will load `config/bootstrap.php` in the plugin.
      parent::bootstrap($app);
    }

    public function routes($routes) {
      // Add routes.
      // By default will load `config/routes.php` in the plugin.
      parent::routes($routes);
    }
  }
