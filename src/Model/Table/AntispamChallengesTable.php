<?php
namespace FinlayDaG33k\AntiSpam\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AntispamChallenges Model
 *
 * @method \FinlayDaG33k\AntiSpam\Model\Entity\AntispamChallenge get($primaryKey, $options = [])
 * @method \FinlayDaG33k\AntiSpam\Model\Entity\AntispamChallenge newEntity($data = null, array $options = [])
 * @method \FinlayDaG33k\AntiSpam\Model\Entity\AntispamChallenge[] newEntities(array $data, array $options = [])
 * @method \FinlayDaG33k\AntiSpam\Model\Entity\AntispamChallenge|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \FinlayDaG33k\AntiSpam\Model\Entity\AntispamChallenge saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \FinlayDaG33k\AntiSpam\Model\Entity\AntispamChallenge patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \FinlayDaG33k\AntiSpam\Model\Entity\AntispamChallenge[] patchEntities($entities, array $data, array $options = [])
 * @method \FinlayDaG33k\AntiSpam\Model\Entity\AntispamChallenge findOrCreate($search, callable $callback = null, $options = [])
 */
class AntispamChallengesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('antispam_challenges');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nonce')
            ->requirePresence('nonce', 'create')
            ->notEmptyString('nonce');

        $validator
            ->scalar('solution')
            ->requirePresence('solution', 'create')
            ->notEmptyString('solution');

        $validator
            ->dateTime('timestamp')
            ->requirePresence('timestamp', 'create')
            ->notEmptyDateTime('timestamp');

        return $validator;
    }
}
