<?php
  use Cake\Routing\Route\DashedRoute;
  use Cake\Routing\Router;

  /**
   * Routes for the api
   */
  Router::plugin('FinlayDaG33k/AntiSpam',['path' => '/antispam', '_namePrefix' => 'FinlayDaG33k/Antispam.'],function ($routes) {
    $routes->get('/getchallenge', ['controller' => 'Api', 'action' => 'getChallenge', '_name' => 'getChallenge']);
    $routes->post('/sendsolution', ['controller' => 'Api', 'action' => 'sendSolution', '_name' => 'sendSolution']);

    $routes->fallbacks(DashedRoute::class);
  });