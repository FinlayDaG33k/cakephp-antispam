<?php
use Migrations\AbstractMigration;

class AddChallengesTable extends AbstractMigration {
  public function change() {
    // Define our table
    $table = $this->table('antispam_challenges');

    // Add our "nonce" field
    $table->addColumn('nonce', 'text', [
      'default' => null,
      'null' => false
    ]);

    // Add the "solution" field
    $table->addColumn('solution', 'text', [
      'default' => null,
      'null' => false
    ]);

    $table->addColumn('timestamp', 'timestamp', [
      'default' => null,
      'null' => false
    ]);

    // Create the table
    $table->save();
  }
}
