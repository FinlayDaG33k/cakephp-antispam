<?php
use Migrations\AbstractMigration;

class AddTokensTable extends AbstractMigration {
  public function change() {
    // Define our table
    $table = $this->table('antispam_tokens');

    // Add our "token" field
    $table->addColumn('token', 'text', [
      'default' => null,
      'null' => false
    ]);

    $table->addColumn('timestamp', 'timestamp', [
      'default' => null,
      'null' => false
    ]);

    // Create the table
    $table->save();
  }
}
