# cakephp-antispam

A privacy-respecting anti-spam measure.  

**disclaimer**: At current stage, this plugin should be used as a mere deterrent, its workings have not been battletested yet.  
It also currently does not truthly sign the tokens, it merely uses the server salt and some hashing to create the token.

## Version Compatibility
Below a table of version compatibility.  
Please note that when a new version is released, support for older versions by the maintainer drop *immediately*.
| Plugin Version | CakePHP Version |
|----------------|-----------------|
| 1.x            | 3.x             |